-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 03 Février 2016 à 17:21
-- Version du serveur :  10.1.9-MariaDB
-- Version de PHP :  7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `store_tb`
--

-- --------------------------------------------------------

--
-- Structure de la table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `items` text NOT NULL,
  `expire_date` datetime NOT NULL,
  `paid` tinyint(4) NOT NULL DEFAULT '0',
  `shipped` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `parent` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `category`, `parent`) VALUES
(1, 'VETEMENT', 0),
(2, 'HIVER', 1);

-- --------------------------------------------------------

--
-- Structure de la table `createurs`
--

CREATE TABLE `createurs` (
  `id` int(11) NOT NULL,
  `createur` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `createurs`
--

INSERT INTO `createurs` (`id`, `createur`) VALUES
(1, 'Zakaria');

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `list_price` decimal(10,2) NOT NULL,
  `createur` varchar(255) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `featured` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `sizes` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `list_price`, `createur`, `categories`, `description`, `featured`, `deleted`, `sizes`, `image`) VALUES
(1, 'hhh', '122.00', '12222.00', '1', '2', 'cool                              ', 1, 0, 'S:12,M:12', '/store/images/products/ebfe3e8d0b646fe7ec088fb7e8297375.jpg'),
(2, 'titre1', '3.00', '4.00', '1', '2', 'ewe          ', 1, 0, '4:3', '/store/images/products/53b62d3e9d1c42d9395b305615996b47.jpg,/store/images/products/dfd181b2f5e3d49b871ac6368f0f7050.jpg,/store/images/products/6a0f608371e6a552c5d82e85f3032f4f.jpg'),
(3, 'e3', '0.00', '0.00', '1', '2', '          333', 1, 0, 'e:', '/store/images/products/27add5ca4aa9c58bb0c954d58457954d.png,/store/images/products/8f7b523fe309541aa238f1882b598cfe.png'),
(4, 'ter', '0.00', '0.00', '1', '2', '          frrw          ', 1, 0, 'f:', '/store/images/products/81957dbb39ed8f9d1f5480e0fdcb9a41.jpg,/store/images/products/0748972b10b4f325c4ddb63c2dddecf8.jpg'),
(5, '3', '3.00', '3.00', '1', '2', '          323', 1, 0, '33:33,33:33', '/store/images/products/dcaa99a346d4c912c3f29e37f5785f1c.png'),
(6, '32', '23.00', '23.00', '1', '2', '32', 1, 0, '23:23,32:32', '/store/images/products/0286f5d7bc5b17fc7edbea910a864cfa.png');

-- --------------------------------------------------------

--
-- Structure de la table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `charge_id` varchar(255) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `street1` varchar(255) NOT NULL,
  `street2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip_code` varchar(50) NOT NULL,
  `country` varchar(175) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `txc_type` varchar(255) NOT NULL,
  `txn_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(175) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(175) NOT NULL,
  `password` varchar(255) NOT NULL,
  `join_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL,
  `permissions` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `join_date`, `last_login`, `permissions`) VALUES
(1, 'Zakaria reziki', 'rezikiz@live.com', '$2y$10$PAmP1f0fct3OyBvGdtP9EO68QBhTt4flz09riZERXmIZJldSmI.xW', '2016-01-21 22:09:09', '2016-02-03 17:15:52', 'admin,editor'),
(2, 'Jacki', 'rezikiz@live.co', '$2y$10$dhfa/vjiUsCuAtisX7zjCOKxBFHdVqh.8KdjLnufKv2jYj67MAuXu', '2016-01-24 19:41:02', '2016-01-24 21:26:15', 'editor');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `createurs`
--
ALTER TABLE `createurs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `createurs`
--
ALTER TABLE `createurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
