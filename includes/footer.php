<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Copyright &copy; Your Website 2014</span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li>
                      <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                  <li><a href="contact.php">Contacter</a></li>
                    <li><a href="about.php">À propos</a></li>
                    <li><a href="#">Conditions générales</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>


    <!-- Bootstrap Core JavaScript -->
    <script src="reso/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="reso/js/classie.js"></script>
    <script src="reso/js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="reso/js/jqBootstrapValidation.js"></script>
    <script src="reso/js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="reso/js/agency.js"></script>

    <!-- Custom unitegallery JavaScript -->
    <!-- jQuery -->
    <script src="reso/js/jquery-11.0.min.js"></script>
    <script type='text/javascript' src='reso/js/ug-theme-compact.js'></script>
    <script type='text/javascript' src='reso/js/unitegallery.min.js'></script>
    <script>

      function detailsmodal(id){
        var data = { "id" : id};
        jQuery.ajax({
          url : 'includes/modal.php',
          method : "post",
          data : data,
          success: function(data){
            jQuery('body').append(data);
            jQuery('#details-modal').modal('toggle');
          },
          error : function(){
            alert("error");
          }
        });
      }

      function update_cart(mode,edit_id,edit_size){
        var data = { "mode" : mode, "edit_id" : edit_id , "edit_size" : edit_size};
        jQuery.ajax({
          url:'admin/parsers/update_cart.php',
          method :"post",
          data : data,
          success:function(){location.reload();},
          error:function(){ alert('error de chargement')},
        });
      }
      function add_to_carte(){
        jQuery('#modal_errors').html("");
        var size =   jQuery('#size').val();
        var quantity =   jQuery('#quantity').val();
        var available =   jQuery('#available').val();
        var error = '';
        var data = jQuery('#add_product_form').serialize();
        if(size == '' || quantity == '' || quantity == 0){
          error += '<p class="text-danger text-center"> Vous devez choisir une taille et la quantitée  </p>';
          jQuery('#modal_errors').html(error);
          return;
        }else if(quantity > available){
          error += '<p class="text-danger text-center">il reste que '+available+'en stock.</p>';
          jQuery('#modal_errors').html(error);
          return;
        }else{
          jQuery.ajax({
            url : 'admin/parsers/add_cart.php',
            method : "post",
            data : data,
            success: function(){
              location.reload();
            },
            error : function(){
              alert("error");
            }
          });
        }
      }

    </script>



</body>

</html>
