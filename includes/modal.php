<?php
require_once '../core/init.php';
$id = $_POST['id'];
$id = (int)$id;
$sql = "SELECT * FROM products WHERE id ='$id'";
$result = $db->query($sql);
$product = mysqli_fetch_assoc($result);
$createur_id = $product['createur'];
$sql = "SELECT createur FROM createurs WHERE id='$createur_id'";
$createur_q = $db->query($sql);
$createur = mysqli_fetch_assoc($createur_q);
$sizestring = $product['sizes'];
$sizestring = rtrim($sizestring,',');
$size_array = explode(',',$sizestring);
?>
<?php ob_start(); ?>
<!--  Modal 1 -->
<div class="portfolio-modal modal fade" id="details-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" onclick="closemodal()" >
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2 class="pull-left"><? echo $product['title']; ?></h2>
                        <div class="clearfix"></div>
                        <div id="gallery" style="display:none;">
                          <?php $photos = explode(',',$product['image']);
                          foreach($photos as $photo):
                          ?>
                          <img class="img-responsive img-centered"
                                src="<?= $photo; ?>"
                                alt="<?= $product['title'];?>"
                                data-image="<?= $photo; ?>"
                         			 data-description="<?= $product['title'];?>">
                          <?php endforeach; ?>
                      	</div>
                        <!--<p class="item-intro text-muted pull-left">...</p>-->

                        <br>
                        <p><?=$product['description']; ?></p>
                        <ul class="list-inline">
                            <li>Créateur : <?= $createur['createur']; ?></li>
                            <li>Prix : <?= mony($product['price']); ?></li>
                        </ul>
                        <div class="clearfix"></div>

                        <form action="add_cart.php" method="post" id="add_product_form">
                          <input type="hidden" name="product_id" value="<?=$id;?>">
                          <input type="hidden" name="available" id="available" value="">
                          <div class="form-groupe">
                            <div class="for form-group">
                              <label for="size"> Taille    : </label>
                               <select name="size" id="size" class="form-control">
                                <option value=""></option>
                                <?php foreach($size_array as $string){
                                  $string_arry = explode(':',$string);
                                  $size = $string_arry[0];
                                  $availbale = $string_arry[1];
                                  if ($availbale > 0){
                                  echo '<option value="'.$size.'" data-availbale="'.$availbale.'">'.$size.'  ('.$availbale.' en stock)</option>';
                                  };
                                }
                                ?>
                              </select>
                            </div>
                            <div class="for form-group">
                              <label for="quantity"> Quantité   : </label>
                              <select name="quantity" id="quantity" class="form-control">
                                <?php for ($i=1; $i < 11; $i++):?>
                                <option value="<?=$i;?>"><?=$i;?></option>
                                <?php endfor; ?>
                              </select>
                            </div>
                          </div>
                        </form>
                        <div class="clearfix"></div>
                        <span id="modal_errors" class="bg-danger"></span>
                        <div class="clearfix"></div>

                        <button type="button" class="btn btn-primary pull-right" onclick="add_to_carte(); return false;"><i class="glyphicon glyphicon-shopping-cart"></i> Ajouter au panier</button>
                        <button type="button" class="btn btn-white pull-left col-md-2" onclick="closemodal()" ><i class="fa fa-times"></i> Annuler</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

  jQuery('#size').change(function(){
    var available = jQuery('#size option:selected').data("availbale");
    jQuery('#available').val(available);
  });
  function closemodal() {
    jQuery('#details-modal').modal('hide');
    setTimeout(function(){
      jQuery('#details-modal').remove();
      jQuery('#details-backdrop').remove();
    },500)
  }
  jQuery("#gallery").unitegallery({
    theme_panel_position: "top"
  });


</script>
<?= ob_get_clean();  ?>
