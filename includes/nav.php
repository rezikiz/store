<?php
$sql = "SELECT * FROM categories WHERE parent =0 ";
$pquery = $db->query($sql);
?>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="index.php">Wowstoor</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden"><a href="#page-top"></a></li>
                <?php while($parent = mysqli_fetch_assoc($pquery)) : ?>
                <?php
                $parent_id=$parent['id'];
                $sql2 ="SELECT * FROM categories WHERE parent ='$parent_id'";
                $cquery =$db->query($sql2);
                ?>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" role="button"  href="#0"><?= $parent['category']; ?> <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <?php while($child =mysqli_fetch_assoc($cquery)) : ?>
                    <li><a href="category.php?cat=<?=$child['id']; ?>"><?=$child['category'];?></a></li>
                    <?php endwhile; ?>
                  </ul>
                </li>
              <?php endwhile; ?>
                <li><a class="page-scroll" href="cart.php"><i class="glyphicon glyphicon-shopping-cart"></i><?=((!empty($item['quantity']))?('<span class="label label-pill label-danger">'.$item['quantity'].'</span>'):'(panier)');  ?></a></li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" role="button"  href="#0"><span class="glyphicon glyphicon-search"></span></a>
                  <ul class="dropdown-menu" role="menu">
                  <li>
                    <form>
                      <input  type="search" placeholder="Cherchez.">
                    </form>
                  </li>
                  </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>

    <!-- /.container-fluid -->
</nav>
