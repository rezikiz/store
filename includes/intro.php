<!-- Header -->
<header>
    <div class="container">
        <div class="intro-text">
          <div class="intro-heading">Bienvenue au Paradis</div>
            <div class="intro-lead-in">Tout ce qui est beau se trouve ici</div>
            <a href="#services" class="page-scroll btn btn-xl">Voir plus</a>
        </div>
    </div>
</header>
