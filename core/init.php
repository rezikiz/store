<?php
header ('Content-type:text/html; charset=utf-8');
$db = mysqli_connect('localhost','Zako','zako','store_tb');
if (mysqli_connect_error()){
  echo 'UNE ERRUURE DE CONNEXION A  LA BDD:'.mysqli_connect_error();
  die();
}
session_start();
require_once $_SERVER['DOCUMENT_ROOT'].'/store/config.php';
require_once BASEURL.'helpers/helpers.php';
require BASEURL.'/vendor/autoload.php';

$cart_id = '';
// chek if cookie exist
if(isset($_COOKIE[CART_COOKIE])){
  $cart_id  = sanitize($_COOKIE[CART_COOKIE]);
}

if (isset($_SESSION['SBUser'])){
  $user_id = $_SESSION['SBUser'];
  $query = $db->query("SELECT * FROM users WHERE id = '$user_id' ");
  $user_data = mysqli_fetch_assoc($query);
  $fn = explode(' ', $user_data['full_name']);
  $user_data['first'] = $fn[0];
  if ($user_data['last'] = ''){
    $user_data['last'] = 'sans';
  }
}


if (isset($_SESSION['access_flash'])){
  echo '<div class="bg-success"><p class="text-success text-center">'.$_SESSION['access_flash'].'</p></div>';
  unset($_SESSION['access_flash']);
}
if (isset($_SESSION['error_flash'])){
  echo '<div class="bg-danger"><p class="text-danger text-center">'.$_SESSION['error_flash'].'</p></div>';
  unset($_SESSION['error_flash']);
}

 /* Modification du jeu de résultats en utf8 */
 if (!mysqli_set_charset($db, "utf8")) {
     printf("Erreur lors du chargement du jeu de caractères utf8 : %s\n", mysqli_error($db));
 }
/* define('BASEURL',(__FILE__)); for live */
/*define('BASEURL','/store/');*/

?>
