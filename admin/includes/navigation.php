<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/store/admin/index.php">Dashboard</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="../index.php">index shop</a></li>
        <li><a href="createurs.php">Créateur</a></li>
        <li><a href="categories.php">Catégorie</a></li>
        <li><a href="products.php">Produit</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" role="button"  href="#">Archive <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="archive.php">Produits suprimés</a></li>
            <li><a href="orders_shipped.php">Produits déja envoyer</a></li>
          </ul>
        </li>

        <?php if(has_permission('admin')) :?>
          <li><a href="users.php"> Users</a></li>
        <?php endif; ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Default</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" role="button"  href="#">Hello <?=$user_data['first'];?> <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="change_password.php">Changer mot de passe</a></li>
            <li><a href="logout.php">Se déconnecter</a></li>
          </ul>
        </li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
