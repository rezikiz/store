  <hr>

    <!-- Footer -->
  <footer>
      <div class="row">
          <div  class="text-center">
              <p>ADMIN SITE</p>
          </div>
      </div>
      <!-- /.row -->
  </footer>
  <script>
    function upadateSizes(){
      var sizeString = '';
      for ( var i = 1; i <= 5; i++) {
        if(jQuery('#size'+i).val() != ''){
          sizeString += jQuery('#size'+i).val()+':'+jQuery('#qty'+i).val()+',';
        }
      }
      jQuery('#sizes').val(sizeString);
    };
    function get_child_options(selected) {
      if(typeof selected == 'undefined'){
        var selected = '';
      };
      var parentID = jQuery('#Parent').val();
      jQuery.ajax({
        url: '/store/admin/parsers/child_categories.php',
        type: 'POST',
        data: {$parentID : parentID, selected: selected},
        success: function(data){
          jQuery('#child').html(data);
        },
        error: function(){alert("Somthing went wrong with child options.")},
      });
    }
    jQuery('select[name="parent"]').change(function(){
      get_child_options();
    });
  </script>
  <!-- Bootstrap core JavaScript -->
  <script src="../dist/js/bootstrap.min.js"></script>
  </body>
</html>
