<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/store/core/init.php';
include 'includes/head.php';
include 'includes/navigation.php';
if(!is_logged_in()){
  login_error_re();
}
// Delete products
if  (isset($_GET['delete'])){
    $id = sanitize($_GET['delete']);
    $db->query("UPDATE products SET deleted = 1 WHERE id = '$id'");
    header('location:products.php');
}
$dbpath ='';
if(isset($_GET['add']) || isset($_GET['edit'])){
$createurQuery = $db->query("SELECT * FROM createurs ORDER BY createur");
$parentQuery = $db->query("SELECT * FROM categories WHERE parent = 0 ORDER BY category");
$title = ((isset($_POST['title']) && $_POST['title'] != '' )?sanitize($_POST['title']):'');
$createur = ((isset($_POST['createur']) && !empty($_POST['createur']))?sanitize($_POST['createur']):'');
$parent = ((isset($_POST['parent']) && !empty($_POST['parent']))?sanitize($_POST['parent']):'');
$category = ((isset($_POST['child']) && !empty($_POST['child']))?sanitize($_POST['child']):'');
$price = ((isset($_POST['price']) && $_POST['price'] != '' )?sanitize($_POST['price']):'');
$list_price = ((isset($_POST['list_price']) && $_POST['list_price'] != '' )?sanitize($_POST['list_price']):'');
$description = ((isset($_POST['description']) && $_POST['description'] != '' )?sanitize($_POST['description']):'');
$sizes = ((isset($_POST['sizes']) && $_POST['sizes'] != '' )?sanitize($_POST['sizes']):'');
$sizes = rtrim($sizes,',');
$saved_image ='';

 if (isset($_GET['edit'])) {
   $edit_id = (int)$_GET['edit'];
   $productResaults = $db->query( "SELECT * FROM products WHERE id = '$edit_id' ");
   $product = mysqli_fetch_assoc($productResaults);
   if(isset($_GET['delete_image'])){
     $images = explode(',',$product['image']);
     $imgi = (int)$_GET['imgi'] - 1;
     $image_url = $_SERVER['DOCUMENT_ROOT'].$images[$imgi];
     unlink($image_url);
     unset($images[$imgi]);
     $imageString = implode(',',$images);
     $db->query("UPDATE products SET image = '{$imageString}' WHERE id = '$edit_id'");
     header('location : products.php?edit='.$edit_id);
     header('location : products.php?edit='.$edit_id);

   }
   $category = ((isset($_POST['child']) && $_POST['child'] != '')?sanitize($_POST['child']):$product['categories']);
   $title = ((isset($_POST['title']) && $_POST['title'] != '')?sanitize($_POST['title']):$product['title']);
   $createur = ((isset($_POST['createur']) && $_POST['createur'] != '')?sanitize($_POST['createur']):$product['createur']);
   $parentQ = $db->query("SELECT * FROM categories WHERE id = '$category'");
   $parentResaults = $parentQ->fetch_assoc();
   $parent = ((isset($_POST['parent']) && $_POST['parent'] != '')?sanitize($_POST['parent']):$parentResaults['parent']);
   $price = ((isset($_POST['price']) && $_POST['price'] != '')?sanitize($_POST['price']):$product['price']);
   $list_price = ((isset($_POST['list_price']))?sanitize($_POST['list_price']):$product['list_price']);
   $description = ((isset($_POST['description']))?sanitize($_POST['description']):$product['description']);
   $sizes = ((isset($_POST['sizes']) && $_POST['sizes'] != '')?sanitize($_POST['sizes']):$product['sizes']);
   $sizes = rtrim($sizes,',');
   $saved_image = (($product['image'] != '')?$product['image']:'');
   $dbpath = $saved_image;
 }
 if (!empty($sizes)){
   $sizeString = sanitize($sizes);
   $sizeString = rtrim($sizeString,',');
   $sizesArray = explode(',',$sizeString);
   $sArray =  array();
   $qArray =  array();
   foreach ($sizesArray as $ss ) {
     $s = explode(':', $ss);
     $sArray[] =  $s[0];
     $qArray[] =  $s[1];
   }
}else{ $sizesArray = array();}
if ($_POST) {
  $errors = array();
  $required =  array('title','price','parent','child','sizes','createur');
  $allowed = array('png','jpeg','jpg','gif');
  $uploadPath = array();
  $tmpLoc = array();
  foreach ($required as $field) {
    if($_POST[$field] == ''){
      $errors[] = "Toutes les cases avec (*) sont obliguatoires. ";
      break;
    }
  }
  $photoCount = count($_FILES['photo']['name']);
  if ($photoCount > 0){
    for($i =0;$i <$photoCount;$i++){
      $name = $_FILES['photo']['name'][$i];
      $nameArray = explode('.',$name);
      $fileName = $nameArray[0];
      $fileExt = $nameArray[1];
      $mime = explode('/',$_FILES['photo']['type'][$i]);
      $mimeType = $mime[0];
      $mimeExt = $mime[1];
      $tmpLoc[] = $_FILES['photo']['tmp_name'][$i];
      $fileSize = $_FILES['photo']['size'][$i];
      $uploadName = md5(microtime().$i).'.'.$fileExt;
      $uploadPath[] = BASEURL.'/images/products/'.$uploadName;
      if($i != 0){
        $dbpath .= ',';
      }
      $dbpath .='/store/images/products/'.$uploadName;
      if ($mimeType != 'image') {
        $errors[]= "Le fichier doit être une photo.";
      }
      if (!in_array($fileExt, $allowed)) {
        $errors[]= "Le type de la photo doit être png, jpeg, jpg, ou gif.";
      }
      if ($fileSize > 2000000){
        $errors[] = "La taille de la photo doit être inferieur à 15MB";
      }
      if($fileExt != $mimeExt && ($mimeExt == 'jpeg' && $fileExt != 'jpg')){
        $errors[] = "File extesion  does  not much the file.";
      }
    }
  }
  if(!empty($errors)){
    echo display_errors($errors);
  }else{
    if($photoCount>0){
      //upload file and insert into db
      for($i =0;$i<$photoCount;$i++){
      move_uploaded_file($tmpLoc[$i],$uploadPath[$i]);
      }
    }
    $insertSql ="INSERT INTO products (`title`,`price`,`list_price`,`createur`,`categories`,`sizes`,`image`,`description`)
    VALUES ('$title','$price','$list_price','$createur','$category','$sizes','$dbpath','$description')";
    if(isset($_GET['edit'])){
      $insertSql ="UPDATE products SET title = '$title', price = '$price', list_price = '$list_price', createur='$createur',
      categories = '$category', sizes = '$sizes', image = '$dbpath', description ='$description' WHERE id='$edit_id'";
    }
    $db->query($insertSql);
    header('location: products.php');
  }
}
?>
<br>
<br>
<br>
<br>
  <div class="container-fluid">
    <h2 class="text-center"><?=((isset($_GET['edit']))?'Edit':'Add A New');?>  Product</h2><hr>
      <form action="products.php?<?=((isset($_GET['edit']))?'edit='.$edit_id:'add=1');?>" method="POST" enctype="multipart/form-data">
        <div class="form-group col-md-4">
          <label for="title">Titre*:</label>
          <input type="text" name="title" class="form-control" id="title" value="<?=$title;?>">
        </div>
        <div class="form-group col-md-4">
          <label for="createur">Créateur*:</label>
          <select class="form-control" id="Createur" name="createur">
            <option value=""<?=(( $createur == '')?' selected':'');?>></option>
            <?php while($c = mysqli_fetch_assoc($createurQuery)):?>
              <option value="<?= $c['id'];?>"<?=(($createur == $c['id'])?' selected':'');?>><?= $c['createur'];?></option>
            <?php endwhile;?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="parent">Catégorie*:</label>
          <select class="form-control" id="Parent" name="parent">
            <option value=""<?=(($parent == '')?'selected':'');?>></option>
            <?php  while($p = mysqli_fetch_assoc($parentQuery)): ?>
              <option value="<?=$p['id'];?>"<?=(($parent == $p['id'])?' selected':'');?>><?= $p['category'];?></option>
            <?php endwhile; ?>
          </select>
        </div>
        <div class="from-group col-md-4">
          <label for="child" >Sous Catégorie*:</label>
          <select id="child" name="child" class="form-control"></select>
        </div>
        <div class="form-group col-md-4">
          <label for="price">Price*</label>
          <input type="text" id="price" name="price"  class="form-control" value="<?=$price;?>">
        </div>
        <div class="form-group col-md-4">
          <label for="price">List Price*</label>
          <input type="list_price" id="list_price" name="list_price"  class="form-control" value="<?=$list_price;?>">
        </div>
        <div class="form-group col-md-4">
          <label>Quantity $ Siezes*:</label>
          <button class="btn btn-default form-control" data-toggle="modal" onclick="jQuery('#sizesModal').modal('toggle');return false;">Quantity $ Siezes </button>
        </div>
        <div class="form-group col-md-4">
          <label>Siezs & Quantity Preview </label>
          <input type="text"  class="form-control" name="sizes" id="sizes" value="<?=$sizes;?>"readonly="">
        </div>
        <div class="form-group col-md-6">
          <?php if($saved_image != ''): ?>
            <?php
            $imgi = 1;
            $images = explode(',',$saved_image);?>
             <?php foreach ($images as $image):?>
            <div class="saved_image col-md-2">
              <img src="<?=$image;?>" alt="saved image">
              <a href="products.php?delete_image=1&edit=<?=$edit_id;?>&imgi=<?=$imgi;?>" class="text-danger">Delete image</a>
            </div>
          <?php
            $imgi++;
            endforeach; ?>
          <?php else: ?>
          <label for="photo">Photos*:</label>
          <input type="file" name="photo[]" id="photo" class="form-control" multiple>
          <?php endif; ?>
        </div>
        <div class="form-group col-md-6">
          <label for="description">Description*:</label>
          <textarea type="text" name="description" id="description" class="form-control" rows="0"><?=$description;?>
          </textarea>
        </div>
        <div class="clearfix"></div>
        <div class="form-group pull-right">
          <a href="products.php" class="btn btn-default">Annuler</a>
          <input type="submit" value="<?=((isset($_GET['edit']))?'Edit':'Add');?> Product" class="btn btn-primary pull-left">
        </div>
        </form>
        <!-- Modal -->
        <div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="sizesModalLabel">Size & Quantity</h4>
              </div>
              <div class="modal-body">
                <div class="container-fluid">
                  <?php for ($i=1; $i <= 7 ; $i++) : ?>
                      <div class="from-group col-md-6">
                        <label for="size<?=$i;?>">Size:</label>
                        <input type="text" name="size<?=$i;?>" id="size<?=$i;?>" value="<?=((!empty($sArray[$i-1]))?$sArray[$i-1]:'');?>" class="form-control">
                      </div>
                      <div class="from-group col-md-6">
                        <label for="qty<?=$i;?>">Quantity:</label>
                        <input type="number" name="qty<?=$i;?>" id="qty<?=$i;?>" value="<?=((!empty($qArray[$i-1]))?$qArray[$i-1]:'');?>" min="0" class="form-control">
                      </div>
                    <?php endfor;?>
                </div><div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                  <button type="button" class="btn btn-primary" onclick="upadateSizes();jQuery('#sizesModal').modal('toggle');return false;">Save changes</button>
                </div>
              </div>
            </div>
          </div>
        </div>
   </div>

    <?php } else {
    $sql = "SELECT * FROM products WHERE deleted=0";
    $presults = $db->query($sql);
    if(isset($_GET['featured'])){
      $id = (int)$_GET['id'];
      $featured = (int)$_GET['featured'];
      $featuredSql = "UPDATE products SET featured = '$featured' WHERE id = '$id'";
      $db->query($featuredSql);
      header('location: products.php');
    }
    ?>
    <div class="container">
      <h2 class="text-center">Products</h2><hr>
      <a href="products.php?add=1" class="btn btn-primary pull-left" id="add-product-btn">Add Product</a>
      <div class="clearfix"></div>
      <br>
      <table class="table table-bordered table-condensed table-striped">
        <thead><th></th><th>Products</th><th>Price</th><th>Category</th><th>Featured</th><th>Sold</th></thead>
        <tbody>
          <?php
            $sql = "SELECT * FROM products WHERE deleted=1";
           ?>
          <?php while($product = mysqli_fetch_assoc($presults)):
              $childID = $product['categories'];
              $catSql = "SELECT * FROM categories WHERE id = '$childID'";
              $result = $db->query($catSql);
              $child = mysqli_fetch_assoc($result);
              $parentID = $child['parent'];
              $pSql = "SELECT * FROM categories WHERE id = '$parentID'";
              $presult = $db->query($pSql);
              $parent = mysqli_fetch_assoc($presult);
              $category = $parent['category'].'~'.$child['category'];
            ?>
            <tr>
              <td>
                <a href="products.php?edit=<?= $product['id'];?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a>
                <a href="products.php?delete=<?= $product['id'];?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-trash"></span></a>
              </td>
              <td><?= $product['title'];?></td>
              <td><?=mony($product['price']);?></td>
              <td><?=$category;?></td>
              <td>
                <a href="products.php?featured=<?=(($product['featured'] == 0)?'1':'0');?>&id=<?=$product['id'];?>" class="btn btn-xs btn-default">
                <span class="glyphicon glyphicon-<?=(($product['featured']==1)?'minus':'plus');?>"></span></a>
                &nbsp <?=(($product['featured'] == 1)?'Featured Products':'');?>
              </td>
              <td>0</td>
            </tr>
          <?php endwhile;?>
        </tbody>
      </table>
    </div>
<?php } ?>
<script>
  jQuery('document').ready(function(){
    get_child_options('<?=$category;?>');
  })
</script>
<?php include 'includes/footer.php' ?>
