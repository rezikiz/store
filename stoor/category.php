<?php
  require_once 'core/init.php';
  include 'includes/head.php';
  include 'includes/nav.php';
  if(isset($_GET['cat'])){
    $cat_id =sanitize($_GET['cat']);
    $category = get_category($cat_id);
  }else{
    $cat_id = '';
  }
  $sql = "SELECT * FROM products WHERE categories ='$cat_id '";
  $productQ = $db->query($sql);
  ?>
<!-- Portfolio Grid Section -->
<section id="portfolio" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><?=$category['child'].' '.$category['parent'];?> </h2>
                <h3 class="section-subheading text-muted">Tout nous <?=$category['child'].' '.$category['parent'];?>.</h3>
            </div>
        </div>
        <div class="row">
          <?php while($product = mysqli_fetch_assoc($productQ)) :?>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a  class="portfolio-link" onclick="detailsmodal(<?php echo $product['id']; ?>)">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <?php $photos = explode(',',$product['image']); ?>
                    <img src="<? echo $photos[0]; ?>" class="img-responsive" alt="<? echo $product['title']; ?>">
                </a>
                <div class="portfolio-caption">
                    <h4><?= mony($product['price']); ?></h4>
                    <p class="text-muted"><?= $product['title']; ?></p>
                </div>
            </div>
          <?php endwhile; ?>
        </div>
    </div>
</section>
<?php include 'includes/footer.php' ?>
