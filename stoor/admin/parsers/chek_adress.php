<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/init.php';
$name = sanitize($_POST['full_name']);
$email = sanitize($_POST['email']);
$street1 = sanitize($_POST['street1']);
$street2 = sanitize($_POST['street2']);
$city = sanitize($_POST['city']);
$country = sanitize($_POST['country']);
$zip_code = sanitize($_POST['zip_code']);
$errors = array();
$required = array(
  'full_name'     => ' Un nom complet',
  'email'         => ' Un email valide',
  'street1'       => ' Une adresse valide ',
  'city'          => ' Une ville',
  'zip_code'      => ' Un code postale',
  'country'       => ' Un pays',
);
/// cheker if all required fileds are filled out

foreach($required as $f => $d) {
  if(empty($_POST[$f]) || $_POST[$f] == ''){
    $errors[] = $d.' est obliguatoire.';
  }
}


// chek if valid  email adress

if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
  $errors[] = 'S\'il vous palit utiliser un email valide';
}

if(!empty($errors)){
  echo display_errors($errors);
}else{

  echo 'passed';

}

?>
