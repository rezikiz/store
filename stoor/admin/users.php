<br>
<br>
<br>
<br>
<?php
    require_once '../core/init.php';
    if(!is_logged_in()){
      login_error_re();
    }
    if(!has_permission('admin')){
      permissions_error_re('index.php');
    }
    include 'includes/head.php';
    include 'includes/navigation.php';
    if(isset($_GET['delete'])){
      $delete_id = sanitize($_GET['delete']);
      $db->query("DELETE FROM users WHERE id = '$delete_id'");
      $_SESSION['success_flash'] = 'users hase been deleted !';
      header('location:users.php');
    }
    if (isset($_GET['add'])){
      $name = ((isset($_POST['name']))?sanitize($_POST['name']):'');
      $email = ((isset($_POST['email']))?sanitize($_POST['email']):'');
      $password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
      $confirm = ((isset($_POST['confirm']))?sanitize($_POST['confirm']):'');
      $permissions = ((isset($_POST['permissions']))?sanitize($_POST['permissions']):'');
      $errors = array();
      if ($_POST) {
        $emailQuery =$db->query("SELECT * FROM users WHERE email= '$email' ");
        $emailCount = mysqli_num_rows($emailQuery);

        if($emailCount != 0){
          $errors[] = 'That email alerady exists in our db';

        }
        $required = array('name','email','password','confirm','permissions');
        foreach ($required as $f) {
          if(empty($_POST[$f])){
            $errors[] ='You must fill out all fields';
            break;
          }
        }
        if(strlen($password)< 6){
          $errors[] ='Your password must be at lest  6 catrecters';
        }
        if($password != $confirm){
          $errors[] =' password does not match';
        }
        if(!filter_var($email,FILTER_VALIDATE_EMAIL)) {
          $errors[] = 'Your must enter a validate email';
        }

        if(!empty($errors)){
          echo display_errors($errors);
        }else{
          $hashed = password_hash($password,PASSWORD_DEFAULT);
          $db->query("INSERT INTO users (full_name,email,password,permissions) VALUES ('$name','$email','$hashed','$permissions')");
          $_SESSION['success_flash'] = 'user hase bee add';
          header('location:users.php');
        }
      }
      ?>
      <div class="container">
        <h2 class="text-center"> add a new user</h2><hr>
        <form action=users.php?add=1 method="post">
          <div class="form-group">
            <label for="name">Full name</label>
            <input type="text" name="name" id="name" class="form-control" value="<?=$name;?>">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email" id="email" class="form-control" value="<?=$email;?>">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control" value="<?=$password;?>">
          </div>
          <div class="form-group">
            <label for="confirm">confirm password</label>
            <input type="password" name="confirm" id="confirm" class="form-control" value="<?=$confirm;?>">
          </div>
          <div class="form-group">
            <label for="permissions" >Permissions</label>
            <select class="form-control" name="permissions" id="permissions">
              <option value=""<?=(($permissions == '')?' selected':'');?>></option>
              <option value="editor"<?=(($permissions == 'editor')?' selected':'');?>>editor</option>
              <option value="admin,editor"<?=(($permissions == 'admin,editor')?' selected':'');?>>admin,editor</option>
            </select>
          </div>
          <div class="form-group">
            <a href="users.php" class="btn btn-default">Cancel</a>
            <input type="submit" value="add user" class="btn btn-primary">
          </div>
        </form>
      </div>

      <?php
    }else{
    $userQuery =$db->query("SELECT * FROM users ORDER BY join_date ")
 ?>
<div class="container">
  <a href="users.php?add" type="submit" class="btn btn-primary pull-left" >Add user</a>
  <hr>
  <div class="panel panel-warning">
<!-- Default panel contents -->
    <div class="panel-heading">Table des utilisateurs</div>
        <table class="table table-bordered table-condensed table-striped">
          <thead><th>#</th><th>Name</th><th>Email</th><th>Join Date</th><th>Last login</th><th>Permissions</th></thead>
          <tbody>
            <?php while($user = mysqli_fetch_assoc($userQuery)):?>
              <tr>
                <td>
                  <?php if($user != $user_data['id']): ?>
                    <a href="users.php?delete=<?=$user['id'];?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-trash"></span></a>
                  <?php endif ?>
                </td>
                <td><?= $user['full_name'];?></td>
                <td><?= $user['email'];?></td>
                <td><?=pretty_date($user['join_date']);?></td>
                <td><?=(($user['last_login']=='0000-00-00 00:00:00')?'never':pretty_date($user['last_login']));?></td>
                <td><?=$user['permissions'];?></td>
              </tr>
            <?php endwhile;?>
          </tbody>
        </table>
    </div>
  </div>
<?php }include 'includes/footer.php';?>
