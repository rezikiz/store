<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/store/core/init.php';
include 'includes/head.php';
$email = ((isset($_POST['email']))?sanitize($_POST['email']):'');
$email = trim($email);
$password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
$password = trim($password);
$errors = array();
?>
<br>
<br>
<br>
<br>
<div id="login-form">
    <div>
      <?php
        if($_POST){
          //form validation
          if (empty($_POST['email']) || empty($_POST['password'])) {
            $errors[] = 'You must provide email and password';
          }
          // validate email
          if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
            $errors[] = 'you must enter a valide email';
          }

          if (strlen($password) < 6){
            $errors[] =' Password must be more than 6 carecters';
          }
          // chek if user exist in the db
            $query =$db->query("SELECT * FROM users WHERE email ='$email'");
            $user = mysqli_fetch_assoc($query);
            $userCount = mysqli_num_rows($query);
            if($userCount < 1) {
              $errors[] = 'That email does not exist un the datebase';
            }
            if(!password_verify($password, $user['password'])){
              $errors[] =' password does not match our recodes';
            }
           // chek for errors
           if(!empty($errors)) {
             echo display_errors($errors);
           }else{
             // log in
             $user_id = $user['id'];
             login($user_id);
           }
        }
      ?>
    </div>
    <form action="login.php" class="form-signin" method="post">
      <h2 class="form-signin-heading">Connexion </h2>
      <label for="email" class="sr-only">Email </label>
      <input type="email" name="email" id="email" value="<?=$email;?>" class="form-control" placeholder="Email " required autofocus>
      <label for="password" class="sr-only">Mot de passe</label>
      <input type="password" name="password" id="password" value="<?=$password;?>" class="form-control" placeholder="Mot de passe" required>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="remember-me"> Se rappeller mes informations
        </label>
      </div>
      <button class="btn btn-md btn-primary " type="submit" value="login">Connexion</button>
      <a class="btn btn-md btn-default " href="../index.php">Acceuil</a>
    </form>
</div>
<?php include 'includes/footer.php'?>
