
<?php

require_once '../core/init.php';

  if (!is_logged_in()){
    header('location: login.php');
  }
include 'includes/head.php';
include 'includes/navigation.php';
// complet order

if (isset($_GET['envoyer']) && $_GET['envoyer'] == 1){
  $cart_id = sanitize((int)$_GET['cart_id']);
  $db->query("UPDATE cart SET shipped = 1 WHERE id = '{$cart_id}'");
  $_SESSION['success_flash'] = "la commande est envoyer";
  header('location:index.php');
}

$txn_id = sanitize((int)$_GET['txn_id']);
$txnQuery = $db->query("SELECT * FROM transactions WHERE id = '{$txn_id}'");
$txn = mysqli_fetch_assoc($txnQuery);
$cart_id = $txn['cart_id'];
$cartQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
$cart = mysqli_fetch_assoc($cartQ);
$items = json_decode($cart['items'],true);
$idArray = array();
$products = array();
foreach($items as $item) {
  $idArray[] = $item['id'];
}
  $ids = implode(',',$idArray);
  $productQ = $db->query(
  "SELECT i.id as 'id', i.title as 'title', c.id 'cid', c.category as 'child', p.category as 'parent'
  FROM products i
  LEFT JOIN categories c ON i.categories = c.id
  LEFT JOIN categories p ON c.parent = p.id
  WHERE i.id IN ({$ids})
  ");
  while ($p = mysqli_fetch_assoc($productQ)) {
    foreach ($items as $item) {
      if($item['id'] == $p['id']){
        $x = $item;
        continue;
      }
    }
    $products[] = array_merge($x,$p);
  }
?>
<br>
<br>
<br>
<br>
<div class="container">
   <h3 class="text-center"> Le(s) produit à envoyer</h3>
    <table class="table table-condensed table-striped table-bordered">
      <thead>
        <th>Quantité</th>
        <th>Titre</th>
        <th>Catégorie</th>
        <th>Taille</th>

      </thead>
      <tbody>
        <?php foreach ($products as $product):?>
        <tr>
          <td><?=$product['quantity'];?></td>
          <td><?=$product['title'];?></td>
          <td><?=$product['parent'].'~'.$product['child'];?></td>
          <td><?=$product['size'];?></td>

        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>


      <div class="col-md-6">
      <div class="row">
      <h3 class="text-center">Détails*</h3>
        <table class="table table-condensed table-striped table-bordered">
          <tbody>
            <tr>
              <td>Total HT</td>
              <td><?=mony($txn['sub_total']);?></td>
            </tr>
            <tr>
              <td>Tax</td>
              <td><?=mony($txn['tax']);?></td>
            </tr>
            <tr>
              <td>Total TTC </td>
              <td><?=mony($txn['grand_total']);?></td>
            </tr>
            <tr>
              <td>Date</td>
              <td><?=pretty_date($txn['txn_date']);?></td>
            </tr>
            <tr>
              <td>Numéro de la transactions</td>
              <td><?=$txn['id'];?></td>
            </tr>
            <tr>
              <td>Numéro de la commande</td>
              <td><?=$txn['cart_id'];?></td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-6">
    <div class="row">
    <h3 class="text-center">Adresse d'envoie*</h3>
      <table class="table table-condensed table-striped table-bordered">
        <tbody>
          <tr>
            <td>Nom et prénom</td>
            <td><?=$txn['full_name'];?></td>
          </tr>
          <tr>
            <td>Adresse 1</td>
            <td><?=$txn['street1'];?></td>
          </tr>
          <?php if($txn['street2']!=''): ?>
          <tr>
            <td>Adresse 2 :</td>
            <td><?=$txn['street2'];?></td>
          </tr>
          <?php endif; ?>
          <tr>
            <td>Code postal:</td>
            <td><?=$txn['zip_code'];?></td>
          </tr>
          <tr>
            <td>Ville:</td>
            <td><?=$txn['city'];?></td>
          </tr>
          <tr>
            <td>Pays:</td>
            <td><?=$txn['country'];?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="pull-left">
    <a href="index.php" class="btn btn-md btn-default">Annuler</a>
  </div>
  <div class="pull-right">
    <a href="orders.php?envoyer=1&cart_id=<?=$cart_id;?>" class="btn btn-md btn-primary">Envoyer</a>
  </div>
</div>


 <?php include 'includes/footer.php' ?>
