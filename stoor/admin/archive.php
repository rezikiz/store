<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/store/core/init.php';
include 'includes/head.php';
include 'includes/navigation.php';
if(!is_logged_in()){
  login_error_re();
}

$sql = " SELECT * FROM Products WHERE deleted =1";
$presults = $db->query($sql);
if  (isset($_GET['remetre'])){
    $id = sanitize($_GET['remetre']);
    $db->query("UPDATE products SET deleted = 0 WHERE id = '$id'");
    header('location:archive.php');
}
?>
<br>
<br>
<br>
<br>
<div class="container">
  <div class="panel panel-warning">
<!-- Default panel contents -->
    <div class="panel-heading">Produits supprimés</div>
        <table class="table table-bordered table-condensed table-striped">
          <thead><th>#</th><th>Produits</th><th>Prix</th><th>Catégories</th></thead>
          <tbody>
            <?php while($product = mysqli_fetch_assoc($presults)):
                $childID = $product['categories'];
                $catSql = "SELECT * FROM categories WHERE id = '$childID'";
                $result = $db->query($catSql);
                $child = mysqli_fetch_assoc($result);
                $parentID = $child['parent'];
                $pSql = "SELECT * FROM categories WHERE id = '$parentID'";
                $presult = $db->query($pSql);
                $parent = mysqli_fetch_assoc($presult);
                $category = $parent['category'].'~'.$child['category'];
              ?>
              <tr>
                <td>
                  <a href="archive.php?remetre=<?= $product['id'];?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-plus"></span></a>
                </td>
                <td><?= $product['title'];?></td>
                <td><?=mony($product['price']);?></td>
                <td><?=$category;?></td>
              </tr>
            <?php endwhile;?>
          </tbody>
        </table>
    </div>
  </div>



<?php include 'includes/footer.php'; ?>
