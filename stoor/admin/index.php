<?php
    require_once '../core/init.php';
    if(!is_logged_in()){
      login_error_re();
    }
    include 'includes/head.php';
    include 'includes/navigation.php';
 ?>

 <?php
  $txnQuery = "SELECT t.id, t.cart_id, t.full_name, t.description, t.txn_date, t.grand_total, c.items, c.paid, c.shipped
  FROM transactions t
  LEFT JOIN cart c ON t.cart_id = c.id
  WHERE c.paid=1 AND c.shipped=0
  ORDER BY t.txn_date";
  $txnResult = $db->query($txnQuery);

  ?>
  <br>
  <br>
  <br>
  <br>
<div class="container">
   <h3 class="text-center"> Commande à envoyer</h3>
   <hr>
   <div class="col-md-12">
   <table class=" table table-condensed table-bordered table-striped">
     <thead>
       <th>Nom</th><th>Déscription</th><th>Total</th><th>Date</th><th>Numéro de la transactions</th><th>#</th>
     </thead>
     <tbody>
       <?php while ($order = mysqli_fetch_assoc($txnResult)) : ?>
       <tr>
         <td><?=$order['full_name'];?></td>
         <td><?=$order['description'];?></td>
         <td><?=mony($order['grand_total']);?></td>
         <td><?=pretty_date($order['txn_date']);?></td>
         <td><?=$order['id'];?></td>
         <td><a href="orders.php?txn_id=<?=$order['id'];?>" class="btn btn-danger">Voir</a></td>
       </tr>
     <?php endwhile;  ?>
     </tbody>
   </table>
 </div>
</div>
 <?php include 'includes/footer.php';?>
