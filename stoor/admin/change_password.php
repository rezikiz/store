<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/store/core/init.php';
if(!is_logged_in()){
  login_error_re();
}
include 'includes/head.php';

$hashed = $user_data['password'];
$old_password = ((isset($_POST['old_password']))?sanitize($_POST['old_password']):'');
$old_password = trim($old_password);
$password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
$password = trim($password);
$confirm = ((isset($_POST['confirm']))?sanitize($_POST['confirm']):'');
$confirm = trim($confirm);
$new_hashed = password_hash($password, PASSWORD_DEFAULT);
$user_id= $user_data['id'];
$errors = array();
?>
<br>
<br>
<br>
<br>
<div id="login-form">
    <div>
      <?php
        if($_POST){
          //form validation
          if (empty($_POST['old_password']) || empty($_POST['password']) || empty($_POST['confirm'])) {
            $errors[] = 'Assurez-vous de bien remplir toutes les cases';
          }

          // password
          if (strlen($password) < 6){
            $errors[] =' le mot de passe doit contenir au moins 6 caractères';
          }
          // chek if new Password exist in the db
            if($password != $confirm){
              $errors = 'les deux mots de passes ne sont pas identiques';
            }
            if(!password_verify($old_password, $hashed)){
              $errors[] = ' Le vieux  mot de passe est faux.';
            }
           // chek for errors
           if(!empty($errors)) {
             echo display_errors($errors);
           }else{
             // change Password
             $db->query("UPDATE users SET password = '$new_hashed' WHERE id = '$user_id' ");
             $_SESSION['success_flash'] = 'Votre mot de passe est modifier avec success';
             header('location: index.php');
           }
        }
      ?>
    </div>
    <form action="change_password.php" class="form-signin" method="post">
      <h2 class="form-signin-heading">Modifer Votre mot de passe  </h2>

      <label for="old_password" class="sr-only">Vieux mot de passe</label>
      <input type="password" name="old_password" id="old_password" value="<?=$old_password;?>" class="form-control" placeholder="Vieux mot de passe" required autofocus>

      <label for="password" class="sr-only">Nouveau mot de passe</label>
      <input type="password" name="password" id="password" value="<?=$password;?>" class="form-control"  placeholder="Nouveau mot de passe" required>

      <label for="confirm" class="sr-only">Confirmer nouveau mot de passe</label>

      <input type="password" name="confirm" id="confirm" value="<?=$confirm;?>" class="form-control" placeholder="Confirmer" required>

      <div class="checkbox">
        <label>
          <input type="checkbox" value="remember-me"> Se rappler de mes informations
        </label>
      </div>

      <button class="btn btn-md btn-primary " type="submit">Enregistrer</button>
      <a class="btn btn-md btn-default " href="../admin/index.php">Accueil</a>

    </form>
</div>
<?php include 'includes/footer.php'?>
