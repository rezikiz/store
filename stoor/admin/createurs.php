<?php
    require_once '../core/init.php';
    include 'includes/head.php';
    include 'includes/navigation.php';
    if(!is_logged_in()){
      login_error_re();
    }
    // get createurs fom db
    $sql = "SELECT * FROM createurs ORDER BY createur";
    $results = $db->query($sql);
    $errors = array();
    // edit createur
    if(isset($_GET['edit']) && !empty($_GET['edit'])){
      $edit_id = (int)$_GET['edit'];
      $edit_id = sanitize($edit_id);
      $sql2 = "SELECT * FROM createurs WHERE id ='$edit_id'";
      $edit_result = $db->query($sql2);
      $ecreateur = mysqli_fetch_assoc($edit_result);
    }
    //delet createur
    if (isset($_GET['delete']) && !empty($_GET['delete'])){
      $delete_id = (int)$_GET['delete'];
      $delete_id = sanitize($delete_id);
      $sql = "DELETE FROM createurs WHERE id ='$delete_id'";
      $db->query($sql);
      header('location: createurs.php');
    }
    // if add form is submitted
    if(isset($_POST['add_submit'])){
      $createur = sanitize($_POST['createur']);
    // chek if createur is blank
      if($_POST['createur'] == ''){
        $errors[] .= 'Vous devez mettre un Nom';
      }
    // chek if createur exist in db
    $sql ="SELECT * FROM createurs WHERE createur = '$createur'";
    if(isset($_GET['edit'])){
      $sql = "SELECT * FROM createurs WHERE id ='$createur' AND id !='$edit_id'";
    }
      $result = $db->query($sql);
      $count = mysqli_num_rows($result);
      if($count > 0){
        $errors[] .= $createur. '  Ce nom existe déjà .';
      }

    // display errors
    if(!empty($errors)){
      echo display_errors($errors);
    }else {
        //add createur to db
        $sql = "INSERT INTO createurs (createur) VALUES ('$createur')";
        if(isset($_GET['edit'])){
          $sql = "UPDATE createurs SET createur = '$createur' WHERE id ='$edit_id'";
        }
        $db->query($sql);
        header('location: createurs.php');
    }

    }

 ?>
 <br>
 <br>
 <br>
 <br>
<div class="container">
  <h2 class="text-center">Créateur</h2> <hr>
  <!-- createur form -->
  <div class="text-center">
    <form  class="form-inline" action="createurs.php<?=((isset($_GET['edit']))?'?edit='.$edit_id:'');?>" method="post">
      <div class="from-group">
        <?php
        $createur_value = '';
        if(isset($_GET['edit'])){
          $createur_value = $ecreateur['createur'];
        }else{
           if(isset($_POST['createur'])){
             $createur_value = sanitize($_POST['createur']);
                  }
              }
        ?>
        <label for="createur"><?=((isset($_GET['edit']))?'':'');  ?>  </label>
        <input type="text" name="createur" id="createur" class="form-control" value="<?= $createur_value; ?>">
        <?php if(isset($_GET['edit'])): ?>
          <a href="createurs.php" class="btn btn-default">Annuler</a>
        <?php endif; ?>
        <input type="submit" name="add_submit" value="<?=((isset($_GET['edit']))?'Modifier  ':'Ajouter  ');?>" class="btn btn-primary">
      </div>
    </form>
  </div>
  <hr>
    <table class="table table-bordered table-striped table-auto">
      <thead>
        <th></th>
        <th>Créateur</th>
        <th></th>
      </thead>
      <tbody>
        <?php while($createur=mysqli_fetch_assoc($results)): ?>
        <td><a href="createurs.php?edit=<?php echo $createur['id']; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>
        <td><?=$createur['createur']; ?></td>
        <td><a href="createurs.php?delete=<?php echo $createur['id']; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-trash"></span></a></td>
      </tbody>
      <?php endwhile; ?>
    </table>
  </div>


<?php
  include 'includes/footer.php';
?>
