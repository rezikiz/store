<?php include 'core/init.php'; ?>
<?php include 'includes/head.php'; ?>
<?php include 'includes/nav.php'; ?>
<?php include 'includes/intro.php';


$sql = "SELECT * FROM products";
$productQ = $db->query($sql);
?>

<section id="portfolio" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <!--<h2 class="section-heading"></h2>-->
                <h3 class="section-subheading text-muted">Tout nous Produit</h3>
            </div>
        </div>
        <div class="row">
          <?php while($product = mysqli_fetch_assoc($productQ)) :?>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a  class="portfolio-link" onclick="detailsmodal(<?php echo $product['id']; ?>)">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <?php $photos = explode(',',$product['image']); ?>
                    <img src="<? echo $photos[0]; ?>" class="img-responsive" alt="<? echo $product['title']; ?>">
                </a>
                <div class="portfolio-caption">
                    <h4><?= mony($product['price']); ?></h4>
                    <p class="text-muted"><?= $product['title']; ?></p>
                </div>
            </div>
          <?php endwhile; ?>
        </div>
    </div>
</section>
<?php include 'includes/footer.php' ?>
