<?php
require_once 'core/init.php';



// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey(STRIPE_PRIVATE);

// Get the credit card details submitted by the form
$token = $_POST['stripeToken'];



$full_name = sanitize($_POST['full_name']);
$email = sanitize($_POST['email']);
$street1 = sanitize($_POST['street1']);
$street2 = sanitize($_POST['street2']);
$city = sanitize($_POST['city']);
$zip_code = sanitize($_POST['zip_code']);
$country = sanitize($_POST['country']);
$tax = sanitize($_POST['tax']);
$city = sanitize($_POST['city']);
$sub_total = sanitize($_POST['sub_total']);
$grand_total = sanitize($_POST['grand_total']);
$cart_id = sanitize($_POST['cart_id']);
$description = sanitize($_POST['description']);
$charge_amount = number_format($grand_total,2) * 100;
$metadata = array(
  "cart_id" => $cart_id,
  "tax"     => $tax,
  "sub_total" =>$sub_total,

);

// adjust inventory
$itemQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
$iresulat = mysqli_fetch_assoc($itemQ);
$items = json_decode($iresulat['items'],true);
foreach ($items as $item) {
  $newSizes = array();
  $item_id =$item['id'];
  $productQ = $db->query("SELECT sizes FROM products WHERE id = '{$item_id}'");
  $product = mysqli_fetch_assoc($productQ);
  $sizes = sizesToArray($product['sizes']);
  foreach ($sizes as $size ) {
    if ($size['size'] == $item['size']){
      $q = $size['quantity'] - $item['quantity'];
      $newSizes[] = array('size' => $size['size'], 'quantity' => $q );
    }else{
      $newSizes[] = array('size' => $size['size'], 'quantity' => $size['quantity']);
    }
  }
  $sizeString = sizesToString($newSizes);
  $db->query("UPDATE products SET sizes = '{$sizeString}' WHERE id = '{$item_id}'");
}


// Create the charge on Stripe's servers - this will charge the user's card
try {
  $charge = \Stripe\Charge::create(array(
    "amount" => $charge_amount, // amount in cents, again
    "currency" => CURRENCY,
    "source" => $token,
    "description" => $description,
    "receipt_email" => $email,
    "metadata"  =>$metadata,
    ));
    $db->query("UPDATE cart SET paid = 1 WHERE id = '{$cart_id}'");
    $db->query("INSERT INTO `transactions`(`id`, `charge_id`, `cart_id`, `full_name`, `street1`, `street2`, `city`, `zip_code`, `country`, `sub_total`, `tax`, `grand_total`, `description`, `txc_type`, `email`) VALUES
       ('','$charge->id','$cart_id','$full_name','$street1','$street2','$city','$zip_code','$country','$sub_total','$tax','$grand_total','$description','$charge->object','$email')");

    $domaine = (($_SERVER['HTTP_HOST'] = 'starofstyle.esy.es')?'.'.$_SERVER['HTTP_HOST']:false);
    setcookie(CART_COOKIE,'',1,'/',$domaine,false);
    include 'includes/head.php';
    ?>
    <div class="container">


      <h2 class="text-centre text-success"> MERCI ! </h2>

      <p class="text-centre"> Votre Payement de la somme de <?=mony($grand_total);?> est accepté,  un mail de confirmation est envoyer.</p>
      <p class="text-centre">Le numéro de votre commande est <strong><?=$cart_id;?></strong>,</p>
      <p class="text-centre">Votre commande sera livrée à l'address ci-dessous</p>
      <address class="text-centre">
        <?=$full_name;?><br>
        <?=$street1;?><br>
        <?=(($street1 != '')?$street2.'<br>':'');?>
        <?=$city?><br>
        <?=$country?><br>
      </address>
    </div>
    <?php
} catch(\Stripe\Error\Card $e) {
  // The card has been declined
  echo $e;
}

 ?>
<?php include 'includes/footer.php' ?>
