<?php
  require_once 'core/init.php';
  include 'includes/head.php';
  include 'includes/nav.php';
  if($cart_id != ''){
    $cartQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
    $result = mysqli_fetch_assoc($cartQ);
    $items = json_decode($result['items'],true);
    $i = 1;
    $sub_total = 0;
    $item_count = 0;
  }
 ?>
 <br>
 <br>
 <br>
 <br>
   <div class="container">
   <div class="col-md-12">
     <div class="row">
       <?php if($cart_id == ''): ?>
         <div class="text-center bg-success">
           <p> Votre panier est vide</p>
         </div>
       <?php else: ?>
         <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">La liste de vous produits</div>

        <!-- Table -->
          <table class="table table-bordered table-condensed table-striped">
            <thead><th>#</th><th>Produit</th><th>Prix</th><th>Quantité</th><th>Taille</th><th>Total HT</th></thead>
            <tbody>
                <?php
                  foreach($items as $item) {
                    $product_id = $item['id'];
                    $productQ = $db->query("SELECT * FROM products WHERE id = '{$product_id}'");
                    $product = mysqli_fetch_assoc($productQ);
                    $sArray = explode(',',$product['sizes']);
                    foreach ($sArray as $sizeString) {
                      $s = explode(':',$sizeString);
                      if($s[0]== $item['size']){
                        $available = $s[1];
                      }
                    }

                  ?>
                <tr>
                  <td><?=$i;?></td>
                  <td><?=$product['title'];?></td>
                  <td><?=mony($product['price']);?></td>
                  <td><?=$item['quantity'];?></td>
                  <td><?=$item['size'];?></td>
                  <td><?=mony($product['price']*$item['quantity']);?></td>
                  <td>
                    <?php if($item['quantity'] < $available):?>
                    <button class="btn btn-xs btn-default" onclick="update_cart('addone','<?=$product['id'];?>','<?=$item['size']?>');"><span class="glyphicon glyphicon-plus"></span></button>
                  <?php else: ?>
                    <span class="text-info">Max</span>
                  <?php endif;?>
                    <button class="btn btn-xs btn-default" onclick="update_cart('removeone','<?=$product['id'];?>','<?=$item['size']?>');"><span class="glyphicon glyphicon-minus"></span></button>
                  </td>
                </tr>
                <?php
                $i++;
                $item_count += $item['quantity'];
                $sub_total += ($product['price']* $item['quantity']);
                $tax = TAXRATE * $sub_total;
                $tax = number_format($tax,2);
                $grand_total = $tax + $sub_total;

              } ?>
            </tbody>
          </table>
          <div class="cleafix"></div>
          <div class="panel-heading">Total TTC (taxe normal  de 20% inclu)<div class="text-right"><?=mony($grand_total);?></div></div>
          <tabel class="table table-bordered table-condensed table-striped">
          </tabel>
      </div>
    </div>
</div>
      <a href="index.php"><span class="btn btn-default btn-lg text-left">Continuer votre shopping</span></a>
      <button type="button" class="btn btn-primary btn-lg text-left" data-toggle="modal" data-target="#chekoutModal">
      Passer la commande
      </button>
     <?php endif; ?>

       <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="chekoutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="chekoutModalLabel"> Sélectionner une adresse d'expédition</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <form action="thankyou.php" method="post" id="payment-form">
                <span class="bg-danger" id="payment-errors"></span>
                <input type="hidden" name="tax" value="<?=$tax;?>">
                <input type="hidden" name="sub_total" value="<?=$sub_total;?>">
                <input type="hidden" name="grand_total" value="<?=$grand_total;?>">
                <input type="hidden" name="cart_id" value="<?=$cart_id;?>">
                <input type="hidden" name="description" value="<?=$item_count.' Produit'.(($item_count >1)?'s':'').'.';?>">
                <div id="step1" style="display:block;">
                  <div class="form-group col-md-6">
                    <label for="full_name"> Nom et prénom : </label>
                    <input class="form-control" id="full_name" name="full_name" type="text">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="email"> Email</label>
                    <input class="form-control" id="email" name="email" type="text">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="street1"> Adresse ligne 1 :  </label>
                    <input class="form-control" id="street1" name="street1" type="text"  data-stripe="address_line1">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="street2"> Adresse ligne 2 </label>
                    <input class="form-control" id="street2" name="street2" type="text"  data-stripe="address_line2">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="city">Ville :</label>
                    <input class="form-control" id="city" name="city" type="text"  data-stripe="address_city">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="zip_code"> Code postal :</label>
                    <input class="form-control" id="zip_code" name="zip_code" type="text"  data-stripe="address_zip">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="country"> Pays :</label>
                    <input class="form-control" id="country" name="country" type="text"  data-stripe="address_country">
                  </div>
                </div>
                <div id="step2" style="display:none;">
                  <div class="form-group col-md-3">
                    <label for="name"> Nom et prénom : </label>
                    <input type="text" id="name" class="form-control" data-stripe="name">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="number"> Numéro de la carte</label>
                    <input type="text" id="number" class="form-control" data-stripe="number">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="cvc">Code</label>
                    <input type="text" size="4" id="cvc" class="form-control" data-stripe="cvc">
                  </div>
                  <div class="form-group col-md-2">
                    <label for="exp-month"> Mois d'expiration</label>
                    <select id="exp-month" class="form-control" data-stripe="exp_month">
                      <option value=""></option>
                      <?php for ($i=1 ; $i < 13 ; $i++ ) : ?>
                        <option value="<?=$i;?>"><?=$i;?></option>
                      <?php endfor; ?>
                      </select>
                  </div>
                  <div class="form-group col-md-2">
                    <label for="exp-year"> Année d'expiration</label>
                    <select id="exp-year" class="form-control"  data-stripe="exp_year">
                      <option value=""></option>
                      <?php $yr=date("Y"); ?>
                      <?php for ($i = 0 ; $i < 12; $i++ ) : ?>
                        <option value="<?=$yr + $i;?>"><?=$yr + $i;?></option>
                      <?php endfor; ?>
                      </select>
                  </div>
            </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
            <button type="button" class="btn btn-primary" onclick="back_adress()" id="back_button" style="display:none;">En arrière</button>
            <button type="button" class="btn btn-primary" onclick="chek_adress()" id="next_button" >Suivant</button>
            <button type="submit" class="btn btn-primary" id="chek_out_button" style="display:none;">Payer</button>
          </div>
          </form>
        </div>
      </div>
    </div>

 <script>
 function back_adress(){
   jQuery('#payment-errors').html("");
   jQuery('#step1').css("display","block");
   jQuery('#step2').css("display","none");
   jQuery('#next_button').css("display","inline-block");
   jQuery('#back_button').css("display","none");
   jQuery('#chek_out_button').css("display","none");
   jQuery('#chekoutModalLabel').html("Sélectionner une adresse d'expédition.");


 }
 function chek_adress(){

   var data = {
     'full_name' : jQuery('#full_name').val(),
     'email' : jQuery('#email').val(),
     'street1' : jQuery('#street1').val(),
     'street2' : jQuery('#street2').val(),
     'city' : jQuery('#city').val(),
     'zip_code' : jQuery('#zip_code').val(),
     'country' : jQuery('#country').val(),
   };
   jQuery.ajax({
    url :'admin/parsers/chek_adress.php',
    method : "POST",
    data : data,
    success : function(data){
        if (data != 'passed'){
          jQuery('#payment-errors').html(data);

        }
        if (data == 'passed'){
          jQuery('#payment-errors').html("");
          jQuery('#step1').css("display","none");
          jQuery('#step2').css("display","block");
          jQuery('#next_button').css("display","none");
          jQuery('#back_button').css("display","inline-block");
          jQuery('#chek_out_button').css("display","inline-block");
          jQuery('#chekoutModalLabel').html("Informations de la cart.");

        }
    },
    error: function(){alert("Erreur de chargement de la page")},
  });
 }



  Stripe.setPublishableKey('<?=STRIPE_PUBLIC;?>');

    function stripeResponseHandler(status, response) {
    var $form = $('#payment-form');

    if (response.error) {
      // Show the errors on the form
      $form.find('#payment-errors').text(response.error.message);
      $form.find('button').prop('disabled', false);
    } else {
      // response contains id and card, which contains additional card details
      var token = response.id;
      // Insert the token into the form so it gets submitted to the server
      $form.append($('<input type="hidden" name="stripeToken" />').val(token));
      // and submit
      $form.get(0).submit();
    }
  };

    jQuery(function($) {
    $('#payment-form').submit(function(event) {
      var $form = $(this);

      // Disable the submit button to prevent repeated clicks
      $form.find('button').prop('disabled', true);

      Stripe.card.createToken($form, stripeResponseHandler);

      // Prevent the form from submitting with the default action
      return false;
    });
  });
 </script>
<?php include 'includes/footer.php'; ?>
